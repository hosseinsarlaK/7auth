<?php

require "bootstrap/init.php";

//echo '<pre>';
//print_r($_POST);
//echo '</pre>';


if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $action = $_GET['action'];
    $params = $_POST;
    if ($action == 'register') {
        # validation data
        if (empty($params['name']) || empty($params['email']) || empty($params['phone'])) {
            setErrorAndRedirect('All input fields required!', 'auth.php?action=register');
        }
        if (!filter_var($params['email'], FILTER_VALIDATE_EMAIL)) {
            setErrorAndRedirect('Enter the valid email address!', 'auth.php?action=register');
        }
        if (!preg_match("~^(?:\+98|0)9(\d{9})$~", $params['phone'])) {
            setErrorAndRedirect("Phone in invalid!", "auth.php?action=register");
        }
        if (isUserExists($params['email'], $params['phone'])) {
            setErrorAndRedirect('User Exists With This Data!', 'auth.php?action=register');
        }
        # requested data is ok
        if (createUser($params)) {
            $_SESSION['email'] = $params['email'];
            redirect('auth.php?action=verify');
        }

    }
}



if (isset($_GET['action']) && $_GET['action'] == 'verify' && !empty($_SESSION['email'])) {

    if (!isUserExists($_SESSION['email']))
        setErrorAndRedirect('User Not Exists With This Data!', 'auth.php?action=login');
    if (isset($_SESSION['hash']) && isAliveToken($_SESSION['hash'])) {
        # send old token
        sendTokenByMail($_SESSION['email'], findTokenByHash($_SESSION['hash']->token));
    } else {
        try {
            $tokenResult = createLoginToken();
            sendTokenByMail($_SESSION['email'], $tokenResult['token']);
        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
        }
        $_SESSION['hash'] = $tokenResult['hash'];
    }
    include 'tpl/verify-tpl.php';
}

if (isset($_GET['action']) && $_GET['action'] == 'register') {
    include 'tpl/register-tpl.php';
} else {
    include "tpl/login-tpl.php";
}

